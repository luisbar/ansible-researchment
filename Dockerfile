# Use the specified Ubuntu image
FROM ubuntu:noble-20240429
# Update package lists
RUN apt-get update
# Install the openssh server for SSH access
RUN apt-get install -y openssh-server
# Create a new user named "luisbar" with a home directory
RUN adduser --disabled-password --shell /bin/bash --home /home/luisbar luisbar
# Set a password for "luisbar" (weak password, not recommended for production)
RUN echo "luisbar:weakpwd" | chpasswd
# Add user "luisbar" to the sudo group for administrative privileges
RUN usermod -a -G sudo luisbar
# Change user to generate SSH key
USER luisbar
# Generate SSH key
RUN ssh-keygen -t rsa -b 4096 -f ~/.ssh/id_rsa 
# Change user to change permissions
USER root
# Copy SSH key generated on host machine
COPY localhost.pub /home/luisbar/.ssh/authorized_keys
# Update permission for copied SSH key
RUN chmod 600 /home/luisbar/.ssh/authorized_keys
# Change user to be used
USER luisbar
# Expose the default SSH port (22)
EXPOSE 22