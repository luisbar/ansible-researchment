## What is?
Ansible provides open-source automation that reduces complexity and runs everywhere. Using Ansible lets you automate virtually any task. Here are some common use cases for Ansible:

- Eliminate repetition and simplify workflows
- Manage and maintain system configuration
- Continuously deploy complex software
- Perform zero-downtime rolling updates

Ansible uses simple, human-readable scripts called playbooks to automate your tasks. You declare the desired state of a local or remote system in your playbook. Ansible ensures that the system remains in that state.

As automation technology, Ansible is designed around the following principles:

- Agent-less architecture
  Low maintenance overhead by avoiding the installation of additional software across IT infrastructure.
- Simplicity
  Automation playbooks use straightforward YAML syntax for code that reads like documentation. Ansible is also decentralized, using SSH existing OS credentials to access to remote machines.
- Scalability and flexibility
  Easily and quickly scale the systems you automate through a modular design that supports a large range of operating systems, cloud platforms, and network devices.
- Idempotence and predictability
  When the system is in the state your playbook describes Ansible does not change anything, even if the playbook runs multiple times.

## Components
![alt](./screenshots/ansible.svg)
As shown in the preceding figure, most Ansible environments have three main components:

- Control node
  A system on which Ansible is installed. You run Ansible commands such as ansible or ansible-inventory on a control node.
- Inventory
  A list of managed nodes that are logically organized. You create an inventory on the control node to describe host deployments to Ansible.
- Managed node
  A remote system, or host, that Ansible controls.

## Inventory
An inventory is a list of manged node or hosts in your infrastructure, you can use group to organized your nodes and You can put each host in more than one group. For example, a production web server in a data center in Atlanta might be included in groups called [prod] and [atlanta] and [webservers]. You can create groups that track:
  - What - An application, stack or microservice (for example, database servers, web servers, and so on).
  - Where - A datacenter or region, to talk to local DNS, storage, and so on (for example, east, west).
  - When - The development stage, to avoid testing on production resources (for example, prod, test).
  ```yml
  ungrouped:
    hosts:
      mail.example.com:
  webservers:
    hosts:
      foo.example.com:
      bar.example.com:
  dbservers:
    hosts:
      one.example.com:
      two.example.com:
      three.example.com:
  east:
    hosts:
      foo.example.com:
      one.example.com:
      two.example.com:
  west:
    hosts:
      bar.example.com:
      three.example.com:
  prod:
    hosts:
      foo.example.com:
      one.example.com:
      two.example.com:
  test:
    hosts:
      bar.example.com:
      three.example.com:
  ```
You can minimize prior yaml file using parent groups, for instance
```yml
ungrouped:
  hosts:
    mail.example.com:
webservers:
  hosts:
    foo.example.com:
    bar.example.com:
dbservers:
  hosts:
    one.example.com:
    two.example.com:
    three.example.com:
east:
  hosts:
    foo.example.com:
    one.example.com:
    two.example.com:
west:
  hosts:
    bar.example.com:
    three.example.com:
prod:
  children:
    east:
test:
  children:
    west:
```
An inventory can be static or dynamic, static means a list of static hosts and dynamic to track cloud services with servers and devices that are constantly starting and stopping

If your `inventory.yaml` file is behemoth you can split it on multiple files and put all of them in a single folder, but you would need to numerate them if you want to load them in certain order, beside that you can move vars to `group_vars` or `host_vars`, so vars will be pulled from those folders, more info about how vars are merged can be find [here](https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html#how-variables-are-merged)

## How to run it?
- Create an ssh key
- Copy the public key on root folder
- Run your containers using `docker-compose up -d`
- Add your host's fingerprint to your `known_hosts` file to manage your host by using this command `ssh -p 6003 localhost -l luisbar`, in that way for all hosts specified on `inventory.yaml` file
- Install `sshpass`
- Run the playbook `ansible-playbook -i inventory --vault-id=development@prompt --vault-id=stage@prompt --vault-id=production@prompt ./playbooks/ping.yaml`

## Tips and tricks
- Verify inventory `ansible-inventory -i inventory.yaml --list`
  - For this repo you have to use `ansible-inventory --vault-id=development@prompt --vault-id=stage@prompt --vault-id=production@prompt -i inventory --list`
- If you change your `ansible_ssh_pass` on your inventory it won't fail after a time because Ansible has a cache and I did not deep into it in order to know how to disable it
- You can encrypt sensitive data using `ansible-vault encrypt_string YOUR_STRING` or `ansible-vault encrypt PATH`, more info [here](https://docs.ansible.com/ansible/latest/tips_tricks/ansible_tips_tricks.html#keep-vaulted-variables-safely-visible)
  - Beside you need a Vault Passwords, more info [here](https://docs.ansible.com/ansible/latest/vault_guide/vault_managing_passwords.html#managing-vault-passwords)
  - If you want to use a single password for all your environments or vars you can use the following command `ansible-playbook -i inventory --ask-vault-pass ./playbooks/ping.yaml`
  - If you want to use different password you have to encrypt by using the `--vault-id` option, here you have the commands used for this project
    - `ansible-vault encrypt --vault-id=development@prompt ./inventory/group_vars/development/vault/ssh.yaml` (password is development)
    - `ansible-vault encrypt --vault-id=stage@prompt ./inventory/group_vars/stage/vault/ssh.yaml` (password is stage)
    - `ansible-vault encrypt --vault-id=production@prompt ./inventory/group_vars/production/vault/ssh.yaml` (password is production)
  - The `--vault-id` option has the following format VAULT_NAME@VAULT_SOURCE
- Links
  - [Ansible docs](https://docs.ansible.com/ansible/latest/index.html)
  - [Module Collection](https://docs.ansible.com/ansible/latest/collections/index.html#list-of-collections)
  - [Install Collection](https://docs.ansible.com/ansible/latest/collections_guide/index.html#collections)
  - [Plugins](https://docs.ansible.com/ansible/latest/plugins/plugins.html#working-with-plugins)
  - [Ansible Galaxy](https://galaxy.ansible.com/)
  - [Ansible Builder](https://ansible.readthedocs.io/projects/builder/en/latest/)
  - [Ansible Navigator](https://ansible.readthedocs.io/projects/navigator/)
  - [Ansible AWX](https://ansible.readthedocs.io/projects/awx/en/latest/userguide/execution_environments.html#use-an-execution-environment-in-jobs)
  - [Ansible Runner](https://ansible.readthedocs.io/projects/runner/en/stable/)
  - VS Code [Ansible](https://marketplace.visualstudio.com/items?itemName=redhat.ansible) and [Dev Containers](https://gitlab.com/luisbar/dev-container-researchment) extensions
  - [Ansible examples](https://github.com/ansible/ansible-examples)
- [Execution Environments](https://docs.ansible.com/ansible/latest/getting_started_ee/index.html) (EEs) are Ansible control nodes packaged as container images. EEs remove complexity to scale out automation projects and make things like deployment operations much more straightforward. EEs provide you with:
  - Software dependency isolation
  - Portability across teams and environments
  - Separation from other automation content and tooling
- You can execute ad hoc command as follow:
  - Without using a module: `ansible development -i inventory --vault-id=development@prompt -a "echo $PATH"`
  - Using a built in module: `ansible development -i inventory --vault-id=development@prompt -m ansible.builtin.shell -a 'echo $PATH'`
  - Ad hoc command are useful for commands you execute sporadically
- Facts represent discovered variables about a system. You can use facts to implement conditional execution of tasks but also just to get ad hoc information about your systems. To see all facts (raw):
  - `ansible all --vault-id=development@prompt --vault-id=stage@prompt --vault-id=production@prompt -i inventory -m ansible.builtin.setup`
- Ansible facts are data related to your remote systems, including operating systems, IP addresses, attached filesystems, and more. You can access this data in the ansible_facts variable. By default, you can also access some Ansible facts as top-level variables with the ansible_ prefix. You can disable this behavior using the INJECT_FACTS_AS_VARS setting. To see all available facts, add this task to a play:
  ```yml
  - name: Print all available facts
    ansible.builtin.debug:
      var: ansible_facts
  ```
- Ansible CLI tools
  - [ansible](https://docs.ansible.com/ansible/latest/cli/ansible.html)
  - [ansible-config](https://docs.ansible.com/ansible/latest/cli/ansible-config.html)
  - [ansible-console](https://docs.ansible.com/ansible/latest/cli/ansible-console.html)
  - [ansible-doc](https://docs.ansible.com/ansible/latest/cli/ansible-doc.html)
  - [ansible-galaxy](https://docs.ansible.com/ansible/latest/cli/ansible-galaxy.html)
  - [ansible-inventory](https://docs.ansible.com/ansible/latest/cli/ansible-inventory.html)
  - [ansible-playbook](https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html)
  - [ansible-pull](https://docs.ansible.com/ansible/latest/cli/ansible-pull.html)
  - [ansible-vault](https://docs.ansible.com/ansible/latest/cli/ansible-vault.html)
- Ansible has an [Ansible extension](https://www.google.com/search?client=safari&rls=en&q=ansible+visual+studio+code&ie=UTF-8&oe=UTF-8) for Visual Studio Code and it has [ansible-lint feature](https://ansible.readthedocs.io/projects/lint/configuring/)
  - To leverage ansible-lint using Ansible extension you have to install it by using the command `pip3 install ansible-lint` then you have to specify the path of the ansible-lint executable on the Ansible extension configuration and you can get it the path by using the command `whereis ansible-lint`
- Most Ansible modules are idempotent, it means they don't execute a command if the desired status was done before
- You can run playbooks in check mode, you only have to add the `-C` flag
- You can use [templates](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_templating.html) to re use logic, Ansible uses Jinja2 as template engine
- [Filters](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_filters.html) let you transform JSON data into YAML data, split a URL to extract the hostname, get the SHA1 hash of a string, add or multiply integers, and much more